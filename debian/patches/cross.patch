From: Helmut Grohne <helmut@subdivi.de>
Date: Sun, 23 Dec 2018 08:02:59 +0100
Subject: Use pkg-config instead of mysql_config

sphinxsearch fails to cross build from source, because it fails
detecting mysql using mysql_config. During cross compilation,
mysql_config does not work at all. For that reason, it should not be
used at all. The attached patch looks up mysql using pkg-config, which
presently is the standard tools for discovering compiler and linker
flags. It makes sphinxsearch cross buildable. Please be aware:
 * In order to use PKG_CHECK_MODULES, it must not occur in a shell
   if/else branch. Such branches must be expressed using AS_IF to make
   the use of AC_REQUIRE work.
 * mariadb presently lacks a dependency on libssl-dev, but it emits
   -lssl as linker flag, see #917135.
 * In order to use the patch, you need to add pkg-config to
   Build-Depends.

I tried writing the patch in a way that is upstreamable as it is useful
to other distributions (e.g. yocto).

Bug-Debian: https://bugs.debian.org/917137
---
 acinclude.m4 | 20 +++++++++++++++-----
 configure.ac | 19 +++++++++----------
 2 files changed, 24 insertions(+), 15 deletions(-)

diff --git a/acinclude.m4 b/acinclude.m4
index 2cb4a05..a9d2dae 100644
--- a/acinclude.m4
+++ b/acinclude.m4
@@ -9,10 +9,10 @@ AC_DEFUN([AC_CHECK_MYSQL],[
 mysqlconfig_locations="mysql_config /usr/bin/mysql_config /usr/local/bin/mysql_config /usr/local/mysql/bin/mysql_config /opt/mysql/bin/mysql_config /usr/pkg/bin/mysql_config"
 user_mysql_includes=
 user_mysql_libs=
+mysqlconfig_used=
 
 # check explicit MySQL root for mysql_config, include, lib
-if test [ x$1 != xyes -a x$1 != xno ]
-then
+AS_IF([test x$1 != xyes -a x$1 != xno],[
 	mysqlroot=`echo $1 | sed -e 's+/$++'`
 	if test [ -x "$mysqlroot/bin/mysql_config" ]
 	then
@@ -41,9 +41,19 @@ then
 	else 
 		AC_MSG_ERROR([invalid MySQL root directory '$mysqlroot'; neither bin/mysql_config, nor include/ and lib/ were found there])
 	fi
-fi
-
+],[
+	PKG_CHECK_MODULES([MYSQL],[mysqlclient],[
+		MYSQL_PKGLIBDIR=`echo $MYSQL_LIBS | sed -e 's/-[[^L]][[^ ]]*//g;s/\s*-L//g;'`
+		mysqlconfig_used=yes
+	],[
+		PKG_CHECK_MODULES([MYSQL],[mariadb],[
+			MYSQL_PKGLIBDIR=`echo $MYSQL_LIBS | sed -e 's/-[[^L]][[^ ]]*//g;s/\s*-L//g;'`
+			mysqlconfig_used=yes
+		],[])
+	])
+])
 
+AS_IF([test "x$mysqlconfig_used" = x],[
 # try running mysql_config
 AC_MSG_CHECKING([for mysql_config])
 for mysqlconfig in $mysqlconfig_locations
@@ -68,11 +78,11 @@ do
 done
 if test [ -n "$mysqlconfig" ]
 then
-	mysqlconfig_used=
 	AC_MSG_RESULT([not found])
 else
 	mysqlconfig_used=yes
 fi
+])
 
 
 # if there's nothing from mysql_config, check well-known include paths
diff --git a/configure.ac b/configure.ac
index 9e1788b..116543d 100644
--- a/configure.ac
+++ b/configure.ac
@@ -344,31 +344,30 @@ AC_ARG_WITH([static-mysql],
 )
 
 AC_MSG_CHECKING([whether to compile with MySQL support])
-if test  x$ac_cv_use_static_mysql != xno -o x$ac_cv_use_mysql != xno
-then
+AS_IF([test x$ac_cv_use_static_mysql != xno -o x$ac_cv_use_mysql != xno],[
 	dl_mysql=0
-	if test x$ac_cv_use_static_mysql != xno ; then
+	AS_IF([test x$ac_cv_use_static_mysql != xno],[
 		AC_MSG_RESULT([static])
     	AC_CHECK_MYSQL([$ac_cv_use_static_mysql])
 		MYSQL_LIBS=`echo $MYSQL_LIBS | sed -e "sX-lmysqlclientX\$MYSQL_PKGLIBDIR/libmysqlclient.aXg"`
-	else
-		if test x$sph_usedl == xyes ; then
+	],[
+		AS_IF([test x$sph_usedl == xyes],[
 			AC_MSG_RESULT([runtime dynamic])
 			AC_CHECK_MYSQL([$ac_cv_use_mysql])
 			MYSQL_LIBS=""
 			dl_mysql=1
-		else
+		],[
 			AC_MSG_RESULT([dynamic])
 			AC_CHECK_MYSQL([$ac_cv_use_mysql])
-		fi
-	fi
+		])
+	])
     AC_DEFINE(USE_MYSQL,1,[Define to 1 if you want to compile with MySQL support])
 	AC_DEFINE_UNQUOTED(DL_MYSQL,$dl_mysql,[Define to 1 if you want runtime load mysql using dlopen])
     AC_SUBST([MYSQL_LIBS])
     AC_SUBST([MYSQL_CFLAGS])
-else
+],[
 	AC_MSG_RESULT([no])
-fi
+])
 AM_CONDITIONAL(USE_MYSQL, test x$ac_cv_use_mysql != xno -o x$ac_cv_use_static_mysql != xno )
 
 dnl ---
